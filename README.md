This tutorial expects working installations of the following software:
- Webstorm (free for students)
- Node.js LTS
- Docker + Docker Compose
- Git
- Vue

# Pre-Setup
If you wish to use a single git repository, initiate it now:  
`/`
```shell
git init
```
Otherways, both `vue create` and `npm init` will create their own git repositories.

# Setup
## Create a new project
- Create an empty project in Webstorm
- Open the terminal in Webstorm and run the following commands (I am using the cmd, since the Node installation seemed the easiest on Windows):

`frontend/`
```shell
# create a new vue app in the current directory
vue create .
# Pick these options:
# ? Please pick a preset: Manually select features                                                                                           
# ? Check the features needed for your project: Router                                 
# ? Choose a version of Vue.js that you want to start the project with 2.x                             
# ? Use history mode for router? (Requires proper server setup for index fallback in production) No
# ? Where do you prefer placing config for Babel, ESLint, etc.? In dedicated config files
# ? Save this as a preset for future projects? (y/N) N

vue add vuetify
# ? Choose a preset: Vuetify 2 - Vue CLI (recommended)
```

# Backend setup
`backend/`
```shell
# create a new node project
npm init --yes

# install express
npm install express
npm install cors
npm install mongodb

# install nodemon (for development)
npm install nodemon --save-dev
```

Add the `.env` file to your project root (use the file from this git).  
Add the `docker-compose.yml` file to your project root (use the file from this git).

## Adding the backend boilerplate
Add a `index.js` file to your `backend/` directory:
```js
const express = require('express');
const app = express();

const cors = require('cors');
app.use(cors({origin: '*'}));

const {getData} = require('./endpoints/data');

const port = process.env.PORT || 5000;


// Serve the frontend
app.use(express.static('public'));

app.get('/data', catchErrors(getData));

app.listen(port, (error) => {
    if (error) return console.error(error);
    console.log(`Backend listening on port ${port}!`);
});

function catchErrors(fn) {
    return function (req, res, next) {
        return fn(req, res, next).catch(err => res.status(500).send({msg: 'Something broke!', err}));
    };
}
```


Add a `db.js` file to your `backend/` directory:
```js
const MongoClient = require('mongodb').MongoClient;

const config = {
    user: process.env.MONGO_USERNAME || 'root',
    password: process.env.MONGO_PASSWORD,
    host: process.env.MONGO_HOSTNAME || 'localhost',
    port: process.env.MONGO_PORT || 27017,
    dbName: process.env.MONGO_DB || 'test'
}

const url = `mongodb://${config.user}:${config.password}@${config.host}:${config.port}/${config.dbName}?authSource=admin`;
const client = new MongoClient(url, {useNewUrlParser: true});

client.db(config.dbName).collection('HenrikIstCool')
    .updateOne({},{$set: {name: 'Henrik', isCool: true}}, {upsert: true}).then();

console.log('Connecting to MongoDB...', url);

async function doSomething() {
    //await client.connect();
    console.log('Connected to MongoDB!');
    const db = client.db(config.dbName);
    const collection = db.collection('HenrikIstCool');
    const result = await collection.find({}).toArray();
    console.log(result);
    //await client.close();
}

module.exports = {
    client,
    doSomething
}
```


Add a `endpoints/data.js` file to your `backend/` directory:
```js
const {client} = require('../db');

const getData = async (req, res) => {
    const result = await client
        .db('test')
        .collection('HenrikIstCool')
        .find({})
        .toArray();

    res.send(result);
}

module.exports = {
    getData
}
```

## Adding the backend start configuration
Add the following scripts to the `package.json`:
```json
{
  "scripts": {
    "backend-dev": "nodemon index.js",
    "start": "node index.js",
    "db-dev": "docker compose up -d mongo"
  }
}
```
Copy the contents of the `.env` file.

Open the `backend/package.json` and right-click the green triangle next to the `backend-dev` script and select `Modify Run Configuration...`, click on the list icon next to `Environment` and click the clipboard icon to paste the contents of the `.env` file.
Change the content of `MONGO_HOSTNAME` to `localhost`.

Run the `db-dev` and `backend-dev` scripts from the menu in the top right corner of your IDE or by clicking the green triangle inside the `package.json` (the scripts are automatically added to the run-menu after running or changing their configuration).

Try accessing the backend at [http://localhost:5000/data](http://localhost:5000/data)`.

# First Frontend steps
In the `App.vue`, replace `v-app-bar div` (ll. 8ff.) with
```vue
<v-toolbar-title>My App</v-toolbar-title>
```
and the content of `v-app-bar v-btn` with
```html
Login
```
and remove the `href` attribute.

Select the `src>components>HelloWorld.vue` file and press Shift+F6 to refactor and name it `QuestionComponent`, make sure the reference search is checked.
Go into the `src>views>HomeView.vue` file and change the `HelloWorld` component to `QuestionComponent`, in the import, components and template. Component names are PascalCase or kebab-case, interchangeably.

Ctrl+Click on the `QuestionComponent` text to go into `src>components>QuestionComponent.vue`, change the name to `QuestionComponent`, delete the contents of the data method within the script tag. Replace the content of the template tag with:
```vue
<v-card>
  <v-card-title>Question</v-card-title>
  <v-card-text>
    {{ question }}
  </v-card-text>
</v-card>
```

## Adding axios
`frontend/` (if you have accidentally installed the module in the backend, use npm remove axios)
```shell
npm install axios
```

Add a new `src>api>index.js` file and add the following code:
```js
import axios from 'axios';

const api = axios.create({
    baseURL: 'http://localhost:5000',
});

api.interceptors.response.use(response => response.data, error => {
    console.error(error);
});

export {api};
```

Integrate the following properties into the export object in the `QuestionComponent`:
```js
import {api} from '@/api'

export default {   
    data: () => ({
      question: null,
    }),
    async mounted() {
      this.question = await api.get('/data')
    },
}
```

Go into the package.json and run the 
`serve` script.
