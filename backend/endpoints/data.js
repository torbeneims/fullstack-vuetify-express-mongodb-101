const {client} = require('../db');

const getData = async (req, res) => {
  const result = await client
      .db('test')
      .collection('HenrikIstCool')
      .find({})
      .toArray();

  res.send(result);
}

module.exports = {
  getData
}
