const MongoClient = require('mongodb').MongoClient;

const config = {
    user: process.env.MONGO_USERNAME || 'root',
    password: process.env.MONGO_PASSWORD,
    host: process.env.MONGO_HOSTNAME || 'localhost',
    port: process.env.MONGO_PORT || 27017,
    dbName: process.env.MONGO_DB || 'test'
}
const url = `mongodb://${config.user}:${config.password}@${config.host}:${config.port}/${config.dbName}?authSource=admin`;
const client = new MongoClient(url, {useNewUrlParser: true});

client.db(config.dbName).collection('HenrikIstCool')
    .updateOne({},{$set: {name: 'Henrik', isCool: true}}, {upsert: true}).then();

console.log('Connecting to MongoDB...', url);

async function doSomething() {
    //await client.connect();
    console.log('Connected to MongoDB!');
    const db = client.db(config.dbName);
    const collection = db.collection('HenrikIstCool');
    const result = await collection.find({}).toArray();
    console.log(result);
    //await client.close();
}

module.exports = {
    client,
    doSomething
}
