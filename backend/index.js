const express = require('express');
const app = express();

const cors = require('cors');
app.use(cors({origin: '*'}));

const {getData} = require('./endpoints/data');

const port = process.env.PORT || 5000;


// Serve the frontend
//app.use(express.static('public'));

app.get('/data', catchErrors(getData));

app.listen(port, (error) => {
    if (error) return console.error(error);
    console.log(`Backend listening on port ${port}!`);
});

function catchErrors(fn) {
    return function (req, res, next) {
        return fn(req, res, next).catch(err => res.status(500).send({msg: 'Something broke!', err}));
    };
}
